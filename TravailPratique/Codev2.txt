; *******************************************************************************
; Programme: code.txt version PEP813 sous Windows
;
;
;       Ce programme prend en entr�e une chaine de caract�res et y retourne un
;       code pour v�rifier la validit� d'un nom de famille.
;
;       auteur:         Alexis Bouchard
;       auteur:         Maxime Masson
;       code permanent: BOUA07049701
;       code permanent: MASM06079507
;       courriel:       bouchard.alexis.2@courrier.uqam.ca
;       courriel:       masson.maxime@courrier.uqam.ca
;       date:           hiver 2019
;       cours:          INF2171
; ******************************************************************************
         NOP0    
         STRO    bienvenu,d
debut:   LDA     0,i         ;
         CHARI   caract,d    ;
         LDBYTEA caract,d    ;
         CPA     32,i        ;
         BREQ    debut       ; v�rifie si caract�re "ENTREE"
         CPA     45,i        ;
         BREQ    dash        ; V�rifie si c'est un tiret
         CPA     10,i        ;
         BREQ    modulo      ;
         ANDA    0x00DF,i    ; mettre en majuscule si minuscule
         SUBA    64,i        ; donner la position dans l'alphabet
         CPA     1,i         ;
         BREQ    multiple    ;
         CPA     5,i         ;
         BREQ    multiple    ;
         CPA     9,i         ;
         BREQ    multiple    ;
         CPA     15,i        ;
         BREQ    multiple    ;
         CPA     21,i        ;
         BREQ    multiple    ;
         CPA     25,i        ;
         BREQ    multiple    ;
continue:ADDA    subtotal,d  
         STA     subtotal,d  ;
         BR      debut       ; recommence
multiple:ASLA                ;
         BR      continue    
modulo:  LDA     subtotal,d  ;
         SUBA    10,i        
suite:   CPA     0,i         
         BRGT    soust       ;
         ADDA    10,i        ;
         BR      fin         
soust:   SUBA    10,i        
         BR      suite       
fin:     STRO    codeval,d   ;
         STA     subtotal,d  
         DECO    subtotal,d  ;
         STRO    point,d     ;
dash:    ADDA    subtotal,d
         STA     subtotal,d
         ADDA    avcaract,d
         STA     avcaract,d
         BR      debut;
avcaract:.BLOCK  1           ; #1d
caract:  .BLOCK  1           ; #1d une lettre du nom de famille
tiret:   .BLOCK  1           ; caractere du tiret
apost:   .BLOCK  1           ; apostrophe
total:   .BLOCK  2           ; #2d
subtotal:.BLOCK  2           ; #2d
codeval: .ASCII  "\n\nLe code de validation est \x00"
point:   .ASCII  ".\X00"
bienvenu:.ASCII  "Entrez un nom de famille valide : \x00"         
         .END                  