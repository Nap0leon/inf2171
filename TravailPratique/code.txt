debut:   LDA       0,i                ; Initialise l'accumulateur
         CHARI     caract,d           ; Lire le charact�re input
         LDBYTEA   caract,d           ; Charger la valeur du charact�re dans l'accumulateur
         CPA       10,i               ; Comparer le charact�re d'entr�e � un "ENTER"
         BREQ      fini               ; Si �gal � entr�, passer � la fin du programme
         CPA       32,i
         BREQ      debut
         ANDA      0xDF,i             ; To uppercase
         SUBA      64,i               ; Valeur selon position de l'alphabet
         CPA       1,i
         BREQ      multipli
         CPA       5,i
         BREQ      multipli
         CPA       9,i
         BREQ      multipli
         CPA       15,i
         BREQ      multipli
         CPA       21,i
         BREQ      multipli
         CPA       25,i
         BREQ      multipli
continu: ADDA      total,d            ; Ajoute total � l'accumulateur
         STA       total,d            ; Store l'accumulateur dans le total
         
         BR        debut
multipli:ASLA                     ; *2
         BR        continu 

fini:    STRO fin,d
         DECO total,d
         STOP    
caract:  .block  2           ; #2d Variable pour charger le caract�re lue en input
total:   .block  2           ; #2d Variable pour charger total  
tiret:   .block  2           ; #2d Variable pour charger tiret
apost:   .block  2           ; #2d Variable pour charger apost
fin:     .ASCII  "\nLe nombre total est : \x00" 

.end








