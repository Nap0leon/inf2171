; *******************************************************************************
;    Programme: lots.txt          version PEP813 sous Windows
;
;    Ce programme analyse les lots gagnants a la loterie
;    Selon l'entree d'un utilisateur.
;
;
;    auteur:         Maxime Masson
;    code permanent: MASM06079507
;    courriel:       masson.maxime@courrier.uqam.ca
;    date:           hiver 2019
;    cours:          INF2171 groupe 10
;
;********************************************************************************
;
; Affichage du message de bienvenue et du mode d'emploi
;
         STRO    msgbien,d   ;    Message de bienvenue du programme
debut:   STRO    msgentre,d  ;
         LDA     0,i         ;    Initialise Accumulateur a
         LDX     0,i         ;    Initialise Index Registre
lecture: CHARI   caract,d    ;    Lecture du charactere a l'entree
         LDA     avcaract,d  ;
         STA     lastval,d   ;
         CPA     "\n",i      ;    Premier caractere est "ENTREE" ?
         BREQ    end         ;    Fin normale du programme
         CPA     " ",i       ;
         BREQ    esp         ;    Si espace, ignore et passe au prochain caract�re
         BR      traitem     ;    Sinon, traitement du charact�re

esp:     CHARI   caract,d    ;    
         LDA     avcaract,d  ;
         CPA     " ",i       ;    Verifie si les charact�res sont que des espaces
         BRNE    esp1        ;
         BR      esp        ;     Sinon, traitement du charact�re
;
;        On passe au traitement des characteres
;
next:    CHARI   caract,d    ;    Lecture du charactere a l'entree
         LDA     avcaract,d  ; 
esp1:    CPA     "\n",i      ;    Fin de ligne ?
         BREQ    fin         ;
traitem: STA     lastval,d   ;    Conserve charact�re comme derni�re valeur
         CPA     " ",i       ;    l'entree est une espace ?
         BREQ    espace      ;    
         LDX     index,d     ;    
         CPX     14,i        ;    Plus de 7 tirage a l'entree ?
         BREQ    invalid     ;    Si oui, annulation des donn�es
         CPA     ".",i       ;    Entr�e est un point ?
         BREQ    point       ;    
         LDX     compoint,d  ;   
         CPX     1,i         ;    L'entr�e est une d�cimale ?
         BREQ    decimale    ;    
         CPX     2,i         ;    L'entr�e est une deuxieme position de d�cimale ? 
         BREQ    invalid     ;    
         CPA     "0",i       ;    valeur < 0 n'est pas un chiffre
         BRLT    invalid     ;    
         CPA     "9",i       ;    valeur > 9 n'est pas un chiffre
         BRGT    invalid     ;    
         STX     comptesp,d  ;    Set compteur espace � 0 car chiffre est rencontr�
         STA     valeur,d    ;    Conserve le charact�re lu dans la variable valeur
         LDA     compteur,d  ;    
         ADDA    1,i         ;
         STA     compteur,d  ;    Passe compteur � true car charact�re accept�e
         LDA     valeur,d    ;
         SUBA    48,i        ;    Transforme l'entr�e en base 10 dans l'accumulateur
         ADDA    calcul,d    ;    Ajoute � variable de calcul
         CALL    FOISDIX     ;    Call sous programme multiplier par 10
         BR      next        ;    Passe au prochain charact�re
;
decimale:STA     valeur,d    ;    
         LDA     compteur,d  ;    
         ADDA    1,i         ;
         STA     compteur,d  ;    Passe compteur � true (1) car chiffre a �t� lu
         LDA     valeur,d    ;
         SUBA    48,i        ;    Transforme l'entree en decimale dans l'accumulateur
         ADDA    calcul,d    ;
         STA     calcul,d    ;    Ajoute decimale a valeur de calcul
         ADDX    1,i         ;
         STX     compoint,d  ;    Ajoute 1 au compteur de position decimale
         LDX     0,i         ;
         STX     comptesp,d  ;
         LDX     compoint,d  ;
         BR      next     
;
verif:   LDA     compteur,d    ;  Si compteur = 0, aucun chiffre n'a �t� lu depuis le dernier point.
         BREQ    invalid     ;
         BR      retour      ;
espace:  LDA     comptesp,d  ;    
         BRGT    skip        ;    Si compteur espace > 0, ignorer l'espace lu
         LDA     compoint,d  ;    Si compteur de point > 0, v�rifier si chiffre a �t� lu
         BRGT    verif       ;
retour:  LDA     calcul,d    ;
         CPA     1000,i      ;    Valeur du tirage > 100 ?
         BRGT    invalid     ;
         CPA     '.',i       ;    Si calcul = '.' -> Deuxieme point lue, invalide
         BREQ    invalid     ;
         LDX     index,d     ;    Charge index tableau dans index registre
         STA     tab,x       ;    Range valeur lue dans tableau
         ADDX    2,i         ;
         STX     index,d     ;    Ajuste nombre de tirage
         ADDA    somme,d     ;    
         STA     somme,d     ;    Ajuste nouvelle somme
         LDA     1,i         ; 
         STA     comptesp,d  ;    Ajuste compteur d'espace
         LDA     0,i         ;
         STA     calcul,d    ;    Reinitialise valeur de calcul
         STA     compoint,d  ;    Reinitialise compteur de point
         STA     compteur,d  ;
skip:    BR      next        ;    Passe au prochain charact�re

;
;
point:   LDX     compoint,d  ;
         CPX     1,i         ;    Deux points de suite ?
         BREQ    invalid     ;
         ADDX    1,i         ;
         STX     compoint,d  ;    Ajoute 1 au compteur de point
         LDA     0,i         ;
         STA     comptesp,d  ;    Reinitialise compteur d'espace
         BR      next        ;    
;
; Si invalid, reinitialise toutes les valeur et oublie le tirage
;
invalid: STRO    msginval,d  ;    Affiche message d'entr�e invalide
         LDA     0,i         ;
         STA     comptesp,d  ;    R�initialise les variables pour oublier tirages courants
         STA     compoint,d  ;
         STA     compteur,d  ;
         STA     calcul,d    ;
         STA     lastval,d   ;
         STA     valeur,d    ;
         STA     index,d     ;
         STA     somme,d     ;
finligne:CHARI   caract,d    ;
         LDA     avcaract,d  ;
         CPA     '\n',i      ;
         BRNE    finligne    ; Vide tampon jusqu'a fin de ligne
         BR      debut       ;
;
;
verif1:  LDA     compteur,d ;     Si compteur = 0, aucun chiffre n'a �t� lu depuis le dernier point.
         BREQ    invalid     ;
         BR      retour1      ;
fin:     LDA     compoint,d  ;    Si compteur de point > 0, v�rifier si chiffre a �t� lu
         BRGT    verif1      ;
retour1: LDA     calcul,d    ;
         CPA     1000,i      ;    Valeur du tirage > 1000 ?
         BRGT    invalid     ;
         CPA     '.',i       ;    Si calcul = '.' -> Deuxieme point lue, invalide
         BREQ    invalid     ;
         LDA     lastval,d   ;    Si derni�re valeur est une espace, ne pas incr�menter l'index
         CPA     " ",i       ;
         BREQ    ignore      ;
         LDA     calcul,d    ;
         LDX     index,d     ;    Charge index du tableau
         STA     tab,x       ;    Range la valeur calculer
         ADDX    2,i         ;    Incr�mente index
         STX     index,d     ;
ignore:  LDA     calcul,d    ;    Charge valeur calcul�
         ADDA    somme,d     ;
         STA     somme,d     ;    Ajuste nouvelle somme
         CALL    NBTIRA      ;    Appelle sous-programme nombre de tirage
         CALL    MAXIMUM     ;    Appelle sous-programme lot maximum
         CALL    MOYENNE     ;    Appelle sous-programme qui calcule la moyenne des lots
         CALL    ECARTYPE    ;    Appelle sous-programme qui calcule l'�cart-type
         LDX     periode,d   ;    
         ADDX    1,i         ;    Incr�mente le nombre de p�riode
         STX     periode,d   ;
         LDX     totaltir,d  ;
         ADDX    nnbtir,d    ;    Incr�mente le nombre de tirage totale du programme
         STX     totaltir,d  ;
         LDA     totalsom,d  ;
         ADDA    somme,d     ;    Ajuste la nouvelle somme des sommes.
         STA     totalsom,d  ;
         LDA     0,i         ;    R�initialise valeurs pour prochaine s�rie de tirage
         STA     comptesp,d  ;    
         STA     compoint,d  ;
         STA     compteur,d  ;
         STA     calcul,d    ;
         STA     lastval,d   ;
         STA     valeur,d    ;
         STA     index,d     ;
         STA     somme,d     ;

         BR      debut        

;*************************************************************
; FIN NORMAL DU PROGRAMME
;
end:     STRO    msgperio,d  ;    Message nombre de periode de tirage
         DECO    periode,d   ;    Affiche nombre de p�riodes total
         STRO    msgtotal,d ;     Message nombre de tirage total
         DECO    totaltir,d  ;    Affiche nombre de tirage total
         STRO    msgttmoy,d  ;    Message de la moyenne des moyennes
         CALL    MOYMOY      ;    Appelle sous-programme qui calcul et affiche la moyenne des moyennes
         STRO    msgfin,d    ;    Message de fin du programme
         STOP 
;
;*************************************************************


;
;        Sous programme qui multiple la valeur de l'accumulateur
;        Par 10.
;
valeurlu:.EQUATE 0           ;    #2d
;      
FOISDIX: SUBSP   2,i         ;    On empile #valeurlu
         STA     valeurlu,d  ;    Store charact�re en entr�e dans valeur 
         ASLA                ;    x2
         ASLA                ;    x4
         ADDA    valeurlu,d    ;    x5
         ASLA                ;    x10
         STA     calcul,d    ;    Store valeur dans calcul
         RET2                ;    On depile #valeurlu
;
;
DIVISDIX:LDX     0,i         ;    Charge index X � 0
soustrai:SUBA    10,i        ;    
         BRLT    ret_arr     ;    Soustrait une fois de trop
         ADDX    1,i         ;
         BR      soustrai    ;   
ret_arr: ADDA    10,i        ;    
         STA     reste,d     ;    Charge reste dans A
         STX     quotient,d  ;    Charge le quotient dans X
         RET0         
;                
NBTIRA:  STRO    msgtir,d    ;    Affiche message nombre tirage
         LDA     index,d     ;    Load nombre tirage dans acc
         ASRA                ;    Divise par 2
         STA     nnbtir,d    ;
         DECO    nnbtir,d    ;    Output nombre de tirage
         RET0 


;
;        Sous programme qui divise la valeur a l'accumulateur par le nombre de tirage
;
diviseu: .EQUATE 0           ;    #2d
diviseur:.EQUATE 2           ;    #2d
;
DIVISE:  SUBSP   4,i         ;    On empile le diviseur #diviseu #diviseur 
         STX     diviseur,s  ;    Charge X comme diviseur
         STX     diviseu,s   ;    
         LDX     diviseu,s   ;
         BREQ    depile      ;    Division par 0 tolere
         LDX     0,i         ;
soustra2:ADDX    1,i 
         SUBA    diviseur,s   ;
         BRLT    ret_arr2    ;    On a soustrait une fois de trop
         BR      soustra2    ;
ret_arr2:SUBX    1,i         ;    Ajuste index
         ADDA    diviseur,s  ;    D�fait la soustraction de trop
         STA     reste,d     ;    Charge accumulateur comme reste
         STX     diviseu,s   ;    Charge index X comme quotient
         CALL    FOISDIX     ;    
         LDX     0,i         ;    Divise le reste qui a �t� multiplier par 10
soustra3:ADDX    1,i         ;
         SUBA    diviseur,s  ;
         BRLT    ret_arr3    ;
         BR      soustra3    ;
ret_arr3:SUBX    1,i         ;
         ADDA    diviseur,s  ;
         CPX     5,i         ;    Si >= 5, arrondir le quotient
         BRLT    skip3       ;
         LDA     diviseu,s   ;
         ADDA    1,i         ;
         STA     quotient,d  ;
         BR      depile      ;
skip3:   LDA     diviseu,s   ; 
         STA     quotient,d  ;
depile:  RET4                ;    On desempile le diviseur #diviseu #diviseur


;
;        Sous programme pour calculer le lot maximum du tirage        
;        
MAXIMUM: STRO    msgmax,d    ;    Affiche message lot maximum
         LDX     index,d     ;    Load index tableau      
next1:   LDA     tab,x       ;
         CPA     plusgr,d    ;    Valeur tableau < que plus grand ?
         BRLT    skip1       ;
         STA     plusgr,d    ;    Charge nouvelle valeur plus grande
skip1:   SUBX    2,i         ;
         CPX     0,i         ;    Pass� au travers de toutes les valeurs du tableau ?
         BRGE    next1       ;
         LDA     plusgr,d    ;    Charge valeur ls plus grande
         CALL    DIVISDIX    ;    Appelle sous-programme pour diviser par 10
         DECO    quotient,d  ;
         CHARO   '.',i       ;
         DECO    reste,d     ;
         RET0                ;    On d�pile et retourne � l'adresse de retour

;
;        Sous programme pour calculer la moyenne des moyennes
; 
MOYMOY:  LDA     moymoy,d    ;    Charge dans A la somme des moyennes
         LDX     periode,d  ;     Charge dans X le nombre de p�riodes
         CALL    DIVISE      ;    Divise la somme des moyennes par le nombre de p�riodes
         CALL    DIVISDIX    ;    Transforme le resultat de divise en d�cimale
         DECO    quotient,d  ;    Affiche le quotient
         CHARO   '.',i       ;
         DECO    reste,d     ;    Affiche la d�cimale
         RET0   
;
;        Sous programme qui calcule la moyenne des lots
;        du tirage courant
; 
MOYENNE: STRO    msgmoy,d    ;    Affiche le message de moyenne des lots
         LDA     somme,d     ;    
         LDX     nnbtir,d    ;
         CALL    DIVISE      ;    Appelle du sous-programme avec parametre A et X
         STA     moyenn,d    ;    Charge le resultat dans la variable moyenne
         LDA     moymoy,d    ;    
         ADDA    moyenn,d    ;
         STA     moymoy,d    ;    Ajoute la moyenne au total des moyennes
         LDA     moyenn,d    ;
         CALL    DIVISDIX    ;    Appelle du sous-programme pour diviser par 10
         DECO    quotient,d  ;
         CHARO   '.',i       ;
         DECO    reste,d     ;
         RET0                ;    On d�pile et retourne � l'adresse de retour
;
;        Sous programme qui calcule l'ecart-type des lots
;        du tirage courant
;
val1:    .EQUATE 0           ;    #2d
somme2:  .EQUATE 2           ;    #2d
ECARTYPE:STRO    msgecart,d  ;
         SUBSP   4,i         ;    On empile #val1 #somme2
         LDA     0,i         ;    Initialise accumulateur � 0 
         STA     somme2,s    ;    
         LDX     index,d     ;    Charge Index dans X
         SUBX    2,i         ;    Ajuste (Additionner 1 fois de trop � la fin du programme)
next2:   LDA     tab,x       ;    Charge valeur du tableau dans A
         STX     index,d     ;
         SUBA    moyenn,d    ;    Soustrait la moyenne
         BRLT    positif     ;    Si n�gatif, branchement pour rendre valeur positive
retour2: STA     val1,s      ;    Range valeur positive dans val1
         LDX     val1,s      ;    Charge val1 dans l'index X pour mettre au carr�
multip:  SUBX    1,i         ;    D�cr�mente X
         ADDA    val1,s       ;   Ajoute val1 � A 
         BRV     debord      ;    Si d�borde, branchement pour annuler.
         CPX     1,i         ;    Fin de la multiplication ?
         BRGT    multip      ;    
         ADDA    somme2,s     ;   Nouvelle somme
         BRV     debord      ;    Si d�borde, branchement pour annuler.
         STA     somme2,s     ;   Range nouvelle somme
         LDX     index,d     ;    
         SUBX    2,i         ;    D�cr�mente index pour prochaine valeur � calculer.
         BRGE    next2       ;
         LDX     nnbtir,d    ;    Charge nombre de tirage dans l'index de registre
         CALL    DIVISE      ;    Appelle sous-programme divise somme2 par nombre de tirages
         CALL    RACINE      ;    Appelle sous-programme pour calculer la racine carr� du r�sultat de DIVISE
         CALL    DIVISDIX    ;    Appelle sous programme de division par 10    
         DECO    quotient,d  ;    Affiche quotient
         CHARO   '.',i       ;
         DECO    reste,d     ;    Affiche la d�cimale du quotient
termine: RET4                ;    On depile #val1 #somme2

positif: NEGA                ;
         BR      retour2     ;
debord:  STRO    nd,d        ; 
         BR      termine     ;
;
;           

;
;        Sous programme qui calcule la racine carree
;        Prend la valeur en parametre A
;
val2:    .EQUATE 0           ;    #2d
RACINE:  SUBSP   2,i         ;    On empile #val2
         STA     val2,s      ;    On sauvegarde le parametre A
         LDA     1,i         ;    
         LDX     1,i         ;
         STA     resultat,d  ;
tantque: CPX     val2,s      ;    Tant que (x <= val2)
         BRGT    finttq      ;
         ADDX    resultat,d  ;
         ADDX    resultat,d  ;
         ADDX    1,i         ;
         ADDA    1,i         ;
         STA     resultat,d  ;
         BR      tantque     ;
finttq:  SUBA    1,i         ;    1 fois en trop
         STA     resultat,d  ;
         RET2                ;    On d�pile #val2
;


               
msgbien: .ASCII  "Bienvenue a ce programme qui analyse les lots"
         .ASCII  "\ngagnants a la loterie."
         .ASCII  "\nLe format des lots doit etre en millions,"
         .ASCII  "\nnumerique, compris entre 0 et 100 inclusivement."
         .ASCII  "\nUne seule position decimale peut etre incluse."
         .ASCII  "\nExemple: (11.1, 2.2, 33, 0.4, 5., .6)"
         .ASCII  "\nLes espaces servent de separateurs et sont"
         .ASCII  "\nacceptes seulement avant ou apres un montant."
         .ASCII  "\nPlusieurs points enchaines ou seulement 1 point"
         .ASCII  "\nne sont pas des donnees acceptees."
         .ASCII  "\nVous devez entrer entre 1 et 7 tirages."
         .ASCII  "\nFaire uniquement la touche ENTREE pour terminer.\x00"
msgentre:.ASCII  "\n\nEntrez les montants des lots gagnants:\n\x00"
msgfin:  .ASCII  "\nFin normale du programme\x00"
msginv:  .ASCII  "\nEntree invalide, veuillez saisir une donnee valide.\x00"
msgtir:  .ASCII  "\n\nNombre de tirages: \x00"
msgmax:  .ASCII  "\nLot maximum: \x00"
msgmoy:  .ASCII  "  Moyenne des lots: \x00"
msgecart:.ASCII  "  Ecart: \x00"
msginval:.ASCII  "\nEntr�e invalide\n\x00"
nd:      .ASCII  "nd\x00"
msgperio:.ASCII  "\n\nNombre de periodes de tirages: \x00"
msgtotal:.ASCII  "\nNombre total de tirages: \x00"
msgttmoy:.ASCII  "\nMoyenne des moyennes des lots gagnants: \x00"
avcaract:.BLOCK  1           ;    #1h
caract:  .BLOCK  1           ;    #1h
comptesp:.BLOCK  2           ;    #2d
compoint:.BLOCK  2           ;    #2d
compteur:.BLOCK  2           ;    #2d
plusgr:  .BLOCK  2           ;    #2d
index:   .BLOCK  2           ;    #2d
nnbtir:  .BLOCK  2           ;    #2d
moyenn:  .BLOCK  2           ;    #2d
somme:   .BLOCK  2           ;    #2d
quotient:.BLOCK  2           ;    #2d
reste:   .BLOCK  2           ;    #2d
resultat:.BLOCK  2           ;    #2D
calcul:  .BLOCK  2           ;    #2d
lastval: .BLOCK  2           ;    #2d
valeur:  .BLOCK  2           ;    #2d
totaltir:.BLOCK  2           ;    #2d
totalsom:.BLOCK  2           ;    #2d
periode: .BLOCK  2           ;    #2d
moymoy:  .BLOCK  2           ;    #2d
tab:     .BLOCK  14          ;    #2d7a
         .END  









