------------------------------------------------------------------------------- ;ERROR: Malformed decimal constant.
      Object
Addr  code   Symbol   Mnemon  Operand     Comment
-------------------------------------------------------------------------------
             ; ************************************************************************************************
             ;       Programme: ASCII4.TXT     version PEP813 sous Windows
             ;
             ;       version 4: 1- n'accepte qu'un seul caract�re � l'entr�e.
             ;                  2- � l'affichage, il affiche 5 caract�res par ligne.
             ;                  3- la valeur en d�cimal occupe 3 positions. (ex: 004, 024)
             ;                  4- les caract�res ascii 0, 9, 10, 13 et 173 ainsi que 128 � 159 sont remplac�s par l'espace.
             ;                  5- tient compte de la position des caract�res et recule s'il y a lieu.
             ;                  6 -le programme recommence tant et aussi longtemps que l'utilisateur
             ;                     n'entre pas le caract�re ENTREE.
             ;
             ;       Ce programme affiche une liste partielle des 256 caract�res ASCII.
             ;
             ;       auteur:         Bernard Martin
             ;       code permanent: non applicable
             ;       courriel:       martin.bernard@uqam.ca
             ;       date:           hiver 2019
             ;       cours:          INF2171
             ; ************************************************************************************************
             ;
             ; affichage du message de bienvenue
             ;
0000  4100DF          STRO    bienvenu,d  ; message d'ent�te
0003  C00001 d�but:   LDA     1,i         ; initialisons les variables
0006  E101D6          STA     compteur,d  
0009  E101D8          STA     pas,d       
000C  41011A          STRO    demande1,d  ; question � l'usager
             ;
             ; saisie du caract�re de d�part
             ;
000F  4901D1          CHARI   caract,d    ; limite inf�rieure
0012  C101D0          LDA     avcaract,d  ; est-ce le ENTREE de fin
0015  B0000A          CPA     "\n",i      
0018  0A00DB          BREQ    final       
001B  4901D5 relire:  CHARI   entr�e,d    ; se d�barrasser du ENTREE
001E  C101D4          LDA     aventr�e,d  ; est-ce un ENTREE
0021  B0000A          CPA     "\n",i      
0024  0C001B          BRNE    relire      ; relire jusqu'au ENTREE
0027  41016B          STRO    demande2,d  ; message � afficher
             ;
             ; saisie du caract�re de terminaison
             ;
002A  4901D3          CHARI   limite,d    ; limite sup�rieure
002D  C101D2          LDA     avlimite,d  
0030  B0000A          CPA     "\n",i      ; si ENTREE alors
0033  0A0042          BREQ    enterlu     ; ENTREE devient le caract�re limite
             ;
0036  4901D5 relire2: CHARI   entr�e,d    ; se d�barrasser du second ENTREE
0039  C101D4          LDA     aventr�e,d  ; est-ce un ENTREE
003C  B0000A          CPA     "\n",i      
003F  0C0036          BRNE    relire2     ; relire jusqu'au ENTREE
             ;
0042  410194 enterlu: STRO    liste,d     ; message � afficher
             ;
0045  C101D0          LDA     avcaract,d  ; caract�re ASCII de d�part
0048  B101D2          CPA     avlimite,d  ; limite inf < limite sup
004B  060054          BRLE    boucle      
004E  C8FFFF          LDX     -1,i        ; on doit reculer
0051  E901D8          STX     pas,d       
0054  25     boucle:  NOP1                ; instruction bidon
0055  C101D0          LDA     avcaract,d  ; caract�re ASCII � traiter
0058  500028          CHARO   '(',i       ; affiche le symbole (
005B  B00064          CPA     100,i       ; chiffre des centaines
005E  0E006D          BRGE    centaine    
0061  500030          CHARO   '0',i       
0064  B0000A          CPA     10,i        ; chiffre des dizaines
0067  0E006E          BRGE    dizaine     
006A  500030          CHARO   '0',i       
006D  25     centaine:NOP1                
006E  3901D0 dizaine: DECO    avcaract,d  ; affiche en d�cimal (2 octets ou WORD, soit avant+caract)
0071  500029          CHARO   ')',i       ; affiche le symbole )
0074  50003D          CHARO   '=',i       ; affiche le symbole =
0077  B00000          CPA     0,i         ; caract�re 0 � ne pas afficher
007A  0A00A7          BREQ    espace      
007D  B00009          CPA     9,i         ; caract�re 9 � ne pas afficher
0080  0A00A7          BREQ    espace      
0083  B0000A          CPA     10,i        ; caract�re 10 � ne pas afficher
0086  0A00A7          BREQ    espace      
0089  B0000D          CPA     13,i        ; caract�re 13 � ne pas afficher
008C  0A00A7          BREQ    espace      
008F  B000AD          CPA     173,i       ; caract�re 173 � ne pas afficher
0092  0A00A7          BREQ    espace      
0095  B0007F          CPA     127,i       ; les caract�res 127 � 159 ne doivent pas �tre affich�s
0098  0800A1          BRLT    affiche     
009B  B0009F          CPA     159,i       
009E  0600A7          BRLE    espace      
00A1  5101D1 affiche: CHARO   caract,d    ; affiche en ASCII (1 octet ou BYTE)
00A4  0400AA          BR      normal      
00A7  500020 espace:  CHARO   ' ',i       ; l'espace remplace les caract�res 9 ou 10
00AA  500020 normal:  CHARO   ' ',i       ; affiche le symbole ESPACE
00AD  C101D6          LDA     compteur,d  
00B0  B00005          CPA     5,i         ; ligne pleine ?
00B3  0A00BC          BREQ    saut        
00B6  700001          ADDA    1,i         
00B9  0400C2          BR      sauve       
00BC  50000A saut:    CHARO   '\n',i      ; effectue un saut de ligne
00BF  C00001          LDA     1,i         
00C2  E101D6 sauve:   STA     compteur,d  
00C5  C101D0          LDA     avcaract,d  ; caract�re ASCII
00C8  B101D2          CPA     avlimite,d  ; limite sup�rieure atteinte ?
00CB  0A00D7          BREQ    termine     ; oui, on termine
00CE  7101D8          ADDA    pas,d       ; caract�re suivant
00D1  F101D1          STBYTEA caract,d    ; on le conserve
00D4  040054          BR      boucle      ; on recommence
00D7  25     termine: NOP1                
00D8  040003          BR      d�but       
             ;
             ; affichage du message de terminaison
             ;
00DB  4101B4 final:   STRO    msgfin,d    ; message de terminaison
00DE  00              STOP                
             ;
             ; variables
             ;                N.B. Il est pr�f�rable d'afficher un long message sur plusieurs lignes car la largeur
             ;                     de la fen�tre d'output de PEP/8 n'est que de 52 caract�res par d�faut.
00DF  426965 bienvenu:.ASCII  "Bienvenue � ce programme d'affichage"
      6E7665 
      6E7565 
      20E020 
      636520 
      70726F 
      677261 
      6D6D65 
      206427 
      616666 
      696368 
      616765 
0103  0A6465          .ASCII  "\ndes caract�res ASCII.\x00"
      732063 
      617261 
      6374E8 
      726573 
      204153 
      434949 
      2E00   
011A  0A0A50 demande1:.ASCII  "\n\nPar quel caract�re,dois-je commencer"
      617220 
      717565 
      6C2063 
      617261 
      6374E8 
      72652C 
      646F69 
      732D6A 
      652063 
      6F6D6D 
      656E63 
      6572   
0140  0A2866          .ASCII  "\n(faire uniquement ENTREE pour terminer)? \x00"
      616972 
      652075 
      6E6971 
      75656D 
      656E74 
      20454E 
      545245 
      452070 
      6F7572 
      207465 
      726D69 
      6E6572 
      293F20 
      00     
016B  0A0A50 demande2:.ASCII  "\n\nPar quel caract�re,dois-je terminer ? \x00"
      617220 
      717565 
      6C2063 
      617261 
      6374E8 
      72652C 
      646F69 
      732D6A 
      652074 
      65726D 
      696E65 
      72203F 
      2000   
0194  0A0A4C liste:   .ASCII  "\n\nListe des caract�res ASCII:\n\n\x00"
      697374 
      652064 
      657320 
      636172 
      616374 
      E87265 
      732041 
      534349 
      493A0A 
      0A00   
01B4  0A0A46 msgfin:  .ASCII  "\n\nFin normale du programme.\x00"
      696E20 
      6E6F72 
      6D616C 
      652064 
      752070 
      726F67 
      72616D 
      6D652E 
      00     
01D0  00     avcaract:.BLOCK  1           ; #1h caract�re de remplissage pour la combinaison "avcaract" et "caract"
01D1  00     caract:  .BLOCK  1           ; #1h
01D2  00     avlimite:.BLOCK  1           ; #1h caract�re de remplissage pour la combinaison "avlimite" et "limite"
01D3  00     limite:  .BLOCK  1           ; #1h
01D4  00     aventr�e:.BLOCK  1           ; #1h caract�re de remplissage pour la combinaison "aventr�e" et "entr�e"
01D5  00     entr�e:  .BLOCK  1           ; #1h capture du ENTREE
01D6  0000   compteur:.BLOCK  2           ; #2d
01D8  0000   pas:     .BLOCK  2           ; #2d
01DA                  .END                  
-------------------------------------------------------------------------------

