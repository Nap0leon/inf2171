; *******************************************************************************
;    Programme: lots.txt          version PEP813 sous Windows
;
;    Ce programme analyse les lots gagnants a la loterie
;    Selon l'entree d'un utilisateur.
;
;
;    auteur:         Maxime Masson
;    code permanent: MASM06079507
;    courriel:       masson.maxime@courrier.uqam.ca
;    date:           hiver 2019
;    cours:          INF2171 groupe 10
;
;********************************************************************************
;
; Affichage du message de bienvenue et du mode d'emploi
;
         STRO    msgbien,d   ;    Message de bienvenue du programme
debut:   STRO    msgentre,d  ;
         LDA     0,i         ;    Initialise Accumulateur
         LDX     0,i         ;    Initialise Index Registre
lecture: CHARI   caract,d    ;    Lecture du charactere a l'entree
         LDA     avcaract,d  ;
         STA     lastval,d   ;
         CPA     "\n",i      ;    Premier caractere est "ENTREE" ?
         BREQ    end         ;
         CPA     " ",i       ;
         BREQ    esp        ;     Si espace, ignore et passe au prochain caract�re
         BR      traitem     ;

esp:     CHARI   caract,d    ;
         LDA     avcaract,d  ;
         CPA     " ",i       ;
         BRNE    esp1        ;
         BR      esp        ;
;
;        On passe au traitement des characteres
;
next:    CHARI   caract,d    ;    Lecture du charactere a l'entree
         LDA     avcaract,d  ;
esp1:    CPA     "\n",i      ;    Fin de ligne ?
         BREQ    fin         ;
traitem: STA     lastval,d   ; 
         CPA     " ",i       ;    l'entree est une espace ?
         BREQ    espace      ;    
         LDX     index,d     ;
         CPX     14,i        ;    Plus de 7 tirage a l'entree ?
         BREQ    invalid     ;    Annulation des entrees
         CPA     ".",i       ;    Entree est un point ?
         BREQ    point       ;
         LDX     compoint,d  ;   
         CPX     1,i         ;    L'entree est une decimale ?
         BREQ    decimale    ;    
         CPX     2,i         ;    L'entree est une deuxieme position de decimale ? 
         BREQ    invalid     ;    
         CPA     "0",i       ;    valeur < 0 n'est pas un chiffre
         BRLT    invalid     ;    
         CPA     "9",i       ;    valeur > 9 n'est pas un chiffre
         BRGT    invalid     ;    
         STX     comptesp,d  ;    Set compteur espace a 0 car chiffre est rencontre
         SUBA    48,i        ;    Transforme l'entree en decimale dans l'accumulateur
         ADDA    calcul,d    ;    Ajoute a variable de calcul
         CALL    FOISDIX     ;    Call sous programme multiplier par 10
         BR      next        ;
;
decimale:SUBA    48,i        ;    Transforme l'entree en decimale dans l'accumulateur
         ADDA    calcul,d    ;
         STA     calcul,d    ;    Ajoute decimale a valeur de calcul
         ADDX    1,i         ;
         STX     compoint,d  ;    Ajoute 1 au compteur de position decimale
         LDX     0,i         ;
         STX     comptesp,d  ;
         LDX     compoint,d  ;
         BR      next     
;
verif:   LDA     calcul,d    ;
         BREQ    invalid
         BR      retour      ;
espace:  LDA     comptesp,d  ;
         BRGT    skip        ;
         LDA     compoint,d  ;
         BRGT    verif       ;
retour:  LDA     calcul,d    ;
         CPA     1000,i      ;    Valeur du tirage > 100 ?
         BRGT    invalid     ;
         CPA     '.',i       ;    Si calcul = '.' -> Deuxieme point lue, invalide
         BREQ    invalid     ;
         LDX     index,d     ;    Charge index tableau dans index registre
         STA     tab,x       ;    Range valeur lue dans tableau
         ADDX    2,i         ;
         STX     index,d     ;    Ajuste nombre de tirage
         ADDA    somme,d     ;    
         STA     somme,d     ;    Ajuste nouvelle somme
         LDA     1,i         ; 
         STA     comptesp,d  ;
         LDA     0,i         ;
         STA     calcul,d    ;    Reinitialise valeur de calcul
         STA     compoint,d  ;    Reinitialise compteur de point
skip:    BR      next        ;

;
;
point:   LDX     compoint,d  ;
         CPX     1,i         ;
         BREQ    invalid     ;
         ADDX    1,i         ;
         STX     compoint,d  ;    Ajoute 1 au compteur de point
         LDA     0,i         ;
         STA     comptesp,d  ;
         BR      next        ;    Lecture prochain caractere
;
; Si invalid, reinitialise toutes les valeur et oublie le tirage
;
invalid: STRO    msginval,d  ;    Affiche message d'entr�e invalide
         LDA     0,i         ;
         STA     compoint,d  ;
         STA     comptesp,d  ;
         STA     index,d     ;
         STA     calcul,d    ;
         STA     somme,d     ;
finligne:CHARI   caract,d    ;
         LDA     avcaract,d  ;
         CPA     '\n',i      ;
         BRNE    finligne    ;
         BR      debut       ;
;
;
verif1:  LDA     calcul,d    ;
         BREQ    invalid     ;
         BR      retour1      ;
fin:     LDA     compoint,d  ;
         BRGT    verif1      ;
retour1: LDA     calcul,d    ;
         CPA     1000,i      ;
         BRGT    invalid     ;
         CPA     '.',i       ;
         BREQ    invalid     ;
         LDA     lastval,d   ;
         CPA     " ",i       ;
         BREQ    ignore      ;
         LDA     calcul,d    ;
         LDX     index,d     ;
         STA     tab,x       ;
         ADDX    2,i         ;
         STX     index,d     ;
ignore:  LDA     calcul,d    ;
         ADDA    somme,d     ;
         STA     somme,d     ;    Somme finale
         CALL    NBTIRA      ;    Call sous-programme nombre de tirage
         CALL    MAXIMUM     ;    Call sous-programme lot maximum
         CALL    MOYENNE     ;
         CALL    ECARTYPE    ;
         LDA     0,i         ;
         STA     comptesp,d  ;
         STA     compoint,d  ;
         STA     calcul,d    ;
         STA     lastval,d   ;
         STA     index,d     ;
         STA     somme,d     ;
         BR      debut        

;*************************************************************
; FIN NORMAL DU PROGRAMME
;
end:     STRO    msgfin,d    ;    Message de fin du programme
         STOP 
;
;*************************************************************


;
;        Sous programme qui multiple la valeur de l'accumulateur
;        Par 10.
;
valeurlu:.EQUATE 0           ;    #2d
;      
FOISDIX: SUBSP   2,i         ;    On empile #valeurlu
         STA     valeurlu,d  ;    Store charact�re en entr�e dans valeur 
         ASLA                ;    x2
         ASLA                ;    x4
         ADDA    valeurlu,d    ;    x5
         ASLA                ;    x10
         STA     calcul,d    ;    Store valeur dans calcul
         RET2                ;    On depile #valeurlu
;
;
DIVISDIX:LDX     0,i         ;
soustrai:SUBA    10,i        ;
         BRLT    ret_arr     ;
         ADDX    1,i         ;
         BR      soustrai    ;   
ret_arr: ADDA    10,i        ;
         STA     reste,d     ;
         STX     quotient,d  ;
         RET0         
;                
NBTIRA:  STRO    msgtir,d    ;    Affiche message nombre tirage
         LDA     index,d     ;    Load nombre tirage dans acc
         ASRA                ;    Divise par 2
         STA     nnbtir,d    ;
         DECO    nnbtir,d    ;    Output nombre de tirage
         RET0 


;
;        Sous programme qui divise la valeur a l'accumulateur par le nombre de tirage
;
diviseu: .EQUATE 0           ;    #2d
;
DIVISE:  SUBSP   2,i         ;    On empile le diviseur #diviseu
         STX     diviseu,s   ;
         LDX     diviseu,s   ;
         BREQ    depile      ;    Division par 0 tolere
         LDX     0,i         ;
soustra2:ADDX    1,i 
         SUBA    nnbtir,d    ;
         BRLT    ret_arr2    ;
         BR      soustra2    ;
ret_arr2:SUBX    1,i         ;
         ADDA    nnbtir,d    ;
         STA     reste,d     ;
         STX     diviseu,s   ;
         CALL    FOISDIX     ;
         LDX     0,i         ;
soustra3:ADDX    1,i         ;
         SUBA    nnbtir,d    ;
         BRLT    ret_arr3    ;
         ADDX    1,i         ;
         BR      soustra3    ;
ret_arr3:SUBX    1,i         ;
         ADDA    nnbtir,d    ;
         CPX     5,i         ;
         BRLT    skip3       ;
         LDA     diviseu,s   ;
         ADDA    1,i         ;
         BR      depile      ;
skip3:   LDA     diviseu,s   ; 
         STA     quotient,d  ;
depile:  RET2                ;    On desempile le diviseur #diviseu


;
;        Sous programme pour calculer le lot maximum du tirage        
;        
MAXIMUM: STRO    msgmax,d    ;    Affiche message lot maximum
         LDX     index,d     ;    Load index tableau      
next1:   LDA     tab,x       ;
         CPA     plusgr,d    ;
         BRLT    skip1       ;
         STA     plusgr,d    ;
skip1:   SUBX    2,i         ;
         CPX     0,i         ;
         BRGE    next1       ;
         LDA     plusgr,d    ;
         CALL    DIVISDIX    ;
         DECO    quotient,d  ;
         CHARO   '.',i       ;
         DECO    reste,d     ;
         RET0
 

;
;        Sous programme qui calcule la moyenne des lots
;        du tirage courant
; 
MOYENNE: STRO    msgmoy,d    ;
         LDA     somme,d     ;
         CALL    DIVISE      ;
         CALL    DIVISDIX    ;
         DECO    quotient,d  ;
         CHARO   '.',i       ;
         DECO    reste,d     ;
         RET0


;
;        Sous programme qui calcule l'ecart-type des lots
;        du tirage courant
; 
ECARTYPE:STRO    msgecart,d  ;
         DECO    0,i         ;
         CHARO   '.',i       ;
         DECO    0,i         ;
         RET0
;
;           



               
msgbien: .ASCII  "Bienvenue a ce programme qui analyse les lots"
         .ASCII  "\ngagnants a la loterie."
         .ASCII  "\nLe format des lots doit etre en millions,"
         .ASCII  "\nnumerique, compris entre 0 et 100 inclusivement."
         .ASCII  "\nUne seule position decimale peut etre incluse."
         .ASCII  "\nExemple: (11.1, 2.2, 33, 0.4, 5., .6)"
         .ASCII  "\nLes espaces servent de separateurs et sont"
         .ASCII  "\nacceptes seulement avant ou apres un montant."
         .ASCII  "\nPlusieurs points enchaines ou seulement 1 point"
         .ASCII  "\nne sont pas des donnees acceptees."
         .ASCII  "\nVous devez entrer entre 1 et 7 tirages."
         .ASCII  "\nFaire uniquement la touche ENTREE pour terminer.\x00"
msgentre:.ASCII  "\n\nEntrez les montants des lots gagnants:\n\x00"
msgfin:  .ASCII  "\nFin normale du programme\x00"
msginv:  .ASCII  "\nEntree invalide, veuillez saisir une donnee valide.\x00"
msgtir:  .ASCII  "\n\nNombre de tirages: \x00"
msgmax:  .ASCII  "\nLot maximum: \x00"
msgmoy:  .ASCII  "  Moyenne des lots: \x00"
msgecart:.ASCII  "  Ecart: \x00"
msginval:.ASCII  "\nEntr�e invalide\n\x00"
avcaract:.BLOCK  1           ;    #1h
caract:  .BLOCK  1           ;    #1h
comptesp:.BLOCK  2           ;    #2d
compoint:.BLOCK  2           ;    #2h
plusgr:  .BLOCK  2           ;    #2d
index:   .BLOCK  2           ;    #2h
nnbtir:  .BLOCK  2           ;    #2h
moyenn:  .BLOCK  2           ;    #2d
somme:   .BLOCK  2           ;    #2d
quotient:.BLOCK  2           ;    #2d
reste:   .BLOCK  2           ;    #2h
calcul:  .BLOCK  2           ;    #2d
lastval: .BLOCK  2           ;    #2d
totaltir:.BLOCK  2           ;    #2d
totalsom:.BLOCK  2           ;    #2d
periode: .BLOCK  2           ;    #2d
tab:     .BLOCK  14          ;    #2d7a
         .END  
